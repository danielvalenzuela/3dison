import { Alert, Grid } from '@mui/material';
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import { Button, Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from '../../hooks';
import { startLoginWithEmailPassword } from '../../store/auth/thunks';
import '../assets/login.css'; // Asegúrate de que la ruta al archivo CSS es correcta


export const LoginPage = () => {

  const { status, errorMessage } = useSelector( state => state.auth );

  const dispatch = useDispatch();

 

  const {email, password, onInputChange} = useForm({
    email:'',
    password:''
  });

  const onSubmit = (event) =>{
    event.preventDefault();
    
    dispatch( startLoginWithEmailPassword({email, password}) );
  }

  return (
    <div className="login-container">
      
      <div className="login-form">
        <h2>Iniciar Sesión</h2>
        <Form onSubmit={onSubmit}>
          <Form.Group className="mb-3">
            <Form.Label htmlFor="email">Email</Form.Label>
            <Form.Control
              type="email"
              name="email"
              id="email"
              value={email}
              onChange={onInputChange}
              required
            />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label htmlFor="password">Contraseña</Form.Label>
            <Form.Control
              type="password"
              name="password"
              id="password"
              value={password}
              onChange={onInputChange}
              required
            />
          </Form.Group>

          <Grid 
              container
              display={ !!errorMessage ? '': 'none' }
              sx={{ mt: 1 }}>
              <Grid 
                  item 
                  xs={ 12 }
                >
                <Alert severity='error'>{ errorMessage }</Alert>
              </Grid>
            </Grid>

          <Button type="submit" className="btn-custom">
            Iniciar Sesión
          </Button>
        </Form>
      </div>
    </div>
  );
};