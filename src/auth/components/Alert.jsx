import React from 'react';
import { Alert } from 'react-bootstrap';


const MyAlert = ({ message }) => {
    return (
      <Alert variant="danger" className="text-center mb-2">
        {message}
      </Alert>
    );
};
  
export default MyAlert;