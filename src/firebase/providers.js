import { FirebaseAuth } from "./firebase";
import { signInWithEmailAndPassword } from 'firebase/auth';


export const loginWithEmailPassword = async({ email, password }) => {

    try {
        const resp = await signInWithEmailAndPassword( FirebaseAuth, email, password );
        const { uid, displayName } = resp.user;

        return {
            ok: true,
            uid, displayName
        }

    } catch (error) {
        return { ok: false, errorMessage: error.message }
    }
}

