// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCNCWSa24cLgUW5FhyYyt9Mzz5MNgYj2Vc",
  authDomain: "dison-7a829.firebaseapp.com",
  projectId: "dison-7a829",
  storageBucket: "dison-7a829.appspot.com",
  messagingSenderId: "361946960449",
  appId: "1:361946960449:web:e98ead218b939db3986cc0",
  measurementId: "G-9HEDJ8RKLQ"
};

// Initialize Firebase
export const FirebaseApp = initializeApp(firebaseConfig);
export const FirebaseAuth = getAuth(FirebaseApp);
export const FirebaseDB = getFirestore(FirebaseApp);

