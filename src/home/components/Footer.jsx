import React, { useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { AiOutlineMail, AiOutlineWhatsApp } from 'react-icons/ai';
import { FaFacebook, FaInstagram, FaSearch } from 'react-icons/fa';
import '../assets/footer.css';

const FooterCus = ({ onSearch }) => {
  const [inputValue, setInputValue] = useState('');
  const scrollToProductGrid = () => {
    const productGridSection = document.querySelector('.product-grid');
    if (productGridSection) {
      productGridSection.scrollIntoView({ behavior: 'smooth' });
    }
  }
  const handleSubmit = (e) => {
    e.preventDefault();

    onSearch(inputValue);
    scrollToProductGrid();
  }
  return (
    <footer className="footer" id="footer">
      <Container fluid style={{ backgroundColor: '#FF5206' }}>
        <Row>

          <Col xs={{ span: 12, order: 2 }} md={{span:3, order:1}}>
            <div className="container">
              <h5>CONTÁCTANOS</h5>
              <a href='mailto:contacto@3dison.tech' className='social-link'><p><AiOutlineMail /> Correo</p></a>
              <a href='https://api.whatsapp.com/send?phone=5214432435436' className='social-link'><p><AiOutlineWhatsApp /> Whatsapp</p></a>
            </div>
          </Col>
          <Col xs={{ span: 12, order: 3 }} md={{span:4, order: 2}}>
            <div className="container">
              <h5>SÍGUENOS</h5>
              <a href='https://www.instagram.com/3dison_tech/' className="social-link"><p><FaInstagram /> 3dison_tech</p></a>
              <a href='https://www.facebook.com/3disonmx' className="social-link"><p><FaFacebook />3disonmx</p></a>
            </div>

          </Col>
          <Col xs={{ span: 12, order: 1 }} md={{span: 5, order:3}}>
              <div className="container">
                    <div className="row height d-flex justify-content-center align-items-center">
                      <div className="col-md-8">
                        <div className="search">
                          <i className="fa fa-search"></i>
                          <form onSubmit={handleSubmit}>
                            <input
                              type="text"
                              className="form-control"
                              placeholder="Buscar En 3Dison..." 
                              value={inputValue}
                              onChange={e => setInputValue(e.target.value)}
                              />
                            <button type="submit"><FaSearch style={{ background: 'transparent', border: 'none', color: 'white' }} /></button>
                          </form>
                        </div>
                      </div>
                    </div>
                </div>
          </Col>

          <Col xs={{ span: 12, order: 4 }} md={12} className='copyrigth'>
            <p className="justify-content-center text-light mb-0">Copyright 2010-2024</p>
          </Col>
        </Row>
      </Container>
      
    </footer>
  );
};

export default FooterCus;