import React, { useState } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { PhoneInput } from 'react-international-phone';
import 'react-international-phone/style.css';
import '../assets/contactForm.css';


const ContactForm = ({ apiUrl }) => {
  const [phone, setPhone] = useState('');

  const [formData, setFormData] = useState({
    nombre: '',
    empresa: '',
    email: '',
    numero: '',
    ciudad: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    // Send formData to the server
    if (apiUrl) {
      try {
        const response = await fetch(apiUrl, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(formData),
        });
        if (response.ok) {
          console.log('Email sent successfully');
        } else {
          console.error('Failed to send email');
        }
      }
      catch (error) {
        console.error('Failed to send email', error);
      }
    } else {
      const mailtoLink = `mailto:mariana.cuevas@3dison.tech?subject=${encodeURIComponent('Formulario de Contacto')}&body=${encodeURIComponent(
        `Nombre: ${formData.nombre}\nCorreo: ${formData.email}\nCiudad: ${formData.ciudad}\nEmpresa: ${formData.empresa}\nNúmero: ${phone}\nSolicito me contacten para realizar un pedido`
      )}`;
      window.location.href = mailtoLink;
    }
  };

  return (
    <Container id="contact" fluid className="custom-container">
      <Row className="justify-content-md-center">
      <h2 style={{ color: '#FF5206' }} className="text-center mb-4">¡HAZ TU PEDIDO AHORA!</h2>
        <Col className="d-flex" >
            <p className="contact-text">Déjanos tus datos y nos pondremos en contacto contigo</p>
        </Col>
        <Col xs={12}>
          
          <Form onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="formBasicName">
              <Form.Control type="text" placeholder="Nombre y apellido" name="nombre" value={formData.nombre} onChange={handleChange}/>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicCompany">
              <Form.Control type="text" placeholder="Empresa (Opcional)" name="empresa" value={formData.empresa} onChange={handleChange}/>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPhone">
              <Form.Label>WhatsApp / Número telefónico:</Form.Label>
              <div >
              <PhoneInput
                inputStyle={{ width: '100%' }}
                defaultCountry="mx"
                value={phone}
                onChange={(phone) => setPhone(phone)}
              />
              </div>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Control type="email" placeholder="Correo *" name="email" value={formData.email} onChange={handleChange} required />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicCity">
              <Form.Control type="text" placeholder="Ciudad *" name="ciudad" value={formData.ciudad} onChange={handleChange} required />
            </Form.Group>
            <Button type="submit">Enviar</Button>

          </Form>
        </Col>
      </Row>
    </Container>
  );
};
export default ContactForm;