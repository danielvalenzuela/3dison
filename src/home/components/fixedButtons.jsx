import React from 'react';
import { Button } from 'react-bootstrap';
import { BsSearch } from 'react-icons/bs';
import { FaWhatsapp } from 'react-icons/fa';
import '../assets/fixedButtons.css';

const FixedButtons = () => {
  const handleSearchIconClick = () => {
    // Scroll to the footer
    document.getElementById('footer').scrollIntoView({ behavior: 'smooth' });

    // Focus on the search input after a slight delay to ensure scrolling finishes
    setTimeout(() => {
      document.querySelector('.footer .form-control').focus();
    }, 500); // Adjust delay as needed
  };

  return (
    <>
    <div className="fixed-buttons-container">
      
      {/* WhatsApp Button with shadow */}
      <Button
        variant="success"
        className="rounded-circle button-custom me-3" // Added 'me-2' class for margin
        href="https://api.whatsapp.com/send?phone=5214432435436" // Replace with your actual WhatsApp number
        target="_blank"
      >
        <FaWhatsapp size={24} />
      </Button>

      {/* Search Button with updated styles */}
      <Button
        variant="light" // Changed to 'light' for white background
        className="rounded-circle button-custom" // Added 'me-2' class for margin
        onClick={handleSearchIconClick} // Implement your search functionality here
      >
        <BsSearch size={24} color="black" /> {/* Changed icon color to black */}
      </Button>


    </div>

    </>
  );
};

export default FixedButtons;
