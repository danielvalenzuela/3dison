import 'bootstrap/dist/css/bootstrap.min.css';
import { collection, getDocs } from 'firebase/firestore';
import React, { useEffect, useState } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import { FirebaseDB } from '../../firebase/firebase';
import '../assets/banner.css';
import useWindowWidth from '../pages/useWindowWidth'; // Adjust the import path as necessary
import MyCustomNavbar from './Navbar';

const CarouselBanner = () => {
    const [banners, setBanners] = useState([]);
    const windowWidth = useWindowWidth(); // Use the custom hook


    useEffect(() => {
        const fetchBanners = async () => {
            const querySnapshot = await getDocs(collection(FirebaseDB, 'banners'));
            const bannersList = querySnapshot.docs.map(doc => ({
                id: doc.id,
                ...doc.data(),
            }));
            setBanners(bannersList);
        };

        fetchBanners();
    }, []);

    const isMobile = windowWidth < 768;

    return (
        <>
            <MyCustomNavbar></MyCustomNavbar>
            <Carousel className="vh-100" controls={false}>
                {banners.map((banner, index) => (
                    <Carousel.Item key={index}>
                        <div style={{
                            backgroundImage: `url(${isMobile ? banner.mobileImageUrl : banner.imageUrl})`,
                            backgroundSize: 'cover',
                            backgroundRepeat: 'no-repeat',
                            backgroundPosition: 'center',
                            width: '100vw',
                            height: '100vh'
                        }}>
                            <div className="d-flex justify-content-center align-items-center h-100">
                            </div>
                        </div>
                    </Carousel.Item>
                ))}
            </Carousel>
        </>
    );
};

export default CarouselBanner;