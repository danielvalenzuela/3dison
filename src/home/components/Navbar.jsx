import React, { useEffect, useState } from 'react';
import { Container, Nav, Navbar, Offcanvas } from 'react-bootstrap';
import { AiOutlineInstagram } from 'react-icons/ai';
import '../assets/navbar_cus.css';


export default function MyCustomNavbar () {
    const [isScrolled, setIsScrolled] = useState(false);

    const checkIfScrolledToProductGrid = () => {
        const productGridOffset = document.getElementById('ProductGrid')?.offsetTop || 0;
        const currentPosition = window.scrollY + window.innerHeight;
        return currentPosition >= productGridOffset;
    };

    useEffect(() => {
        const handleScroll = () => {
            const position = window.scrollY;
            const navbar = document.querySelector('.navbar'); // Select the navbar
            const navbarHeight = navbar.offsetHeight; // Get the dynamic height of the navbar
            const bannerHeight = document.querySelector('.carousel-inner').offsetHeight; // Asegúrate de que este selector coincida con el contenedor de tus banners
            if (position >= (bannerHeight - navbarHeight)) { // need Help here to consider the navbar height and substract it to the bannerHeight
                setIsScrolled(true);
            } else {
                setIsScrolled(false);
            }
        };

        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);
    return (
        <>
        <Navbar  fixed="top" expand={false} className={isScrolled ? 'scrolled-navbar' : ''}>
            <Container fluid className="position-relative navbar-custom-container">
                <Navbar.Brand href="#" className="mx-auto navbar-brand">
                    <img
                        src="../assets/images/3DISON.png" // Reemplaza con el path a tu logo.
                        className="d-inline-block align-center nav-logo"
                        alt="Logo"
                        style={isScrolled ? { width: '217px', height: 'auto' } : {}}
                        /*style={{ width: '217px', height: 'auto' }}*/
                    />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="offcanvasNavbar" className="custom-toggler"/>
                <Navbar.Offcanvas
                    id="offcanvasNavbar"
                    aria-labelledby="offcanvasNavbarLabel"
                    placement="end"
                >
                    <Offcanvas.Header closeButton style={{ backgroundColor: '#20689A', color: 'white'}}>
                    <Offcanvas.Title id="offcanvasNavbarLabel"></Offcanvas.Title>
                        </Offcanvas.Header>
                    <Offcanvas.Body className="d-flex flex-column text-right">
                    <Nav className="flex-grow-1 align-items-end">
                        <Nav.Link className='active' href="#Home">INICIO</Nav.Link>
                        <Nav.Link className='active' href="#filaments">FILAMENTO</Nav.Link>
                        <Nav.Link className='active' href="#contact">CONTACTO</Nav.Link>
                    </Nav>
                    <div className="mt-auto pt-3">
                        <Nav.Link href="#" className="d-flex justify-content-end">SÍGUENOS</Nav.Link>
                            <Nav.Link href="https://instagram.com/3DISON" className="d-flex justify-content-end">
                        <AiOutlineInstagram className="me-2" /> 3DISON
                            </Nav.Link>
                        <Nav.Link href="https://instagram.com/3DISON" className="d-flex justify-content-end">
                            <AiOutlineInstagram className="me-2" /> 3DISON
                        </Nav.Link>
                    </div>
                    </Offcanvas.Body>
                </Navbar.Offcanvas>
            </Container>
            </Navbar>
        </>
    );
}
