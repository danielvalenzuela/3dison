import React, { useState } from 'react';
import CarouselBanner from "../components/Banner";
import ContactForm from "../components/ContactForm";
import FooterCus from "../components/Footer";
import FixedButtons from "../components/fixedButtons";
import ProductGrid from "./Product";


const bannersData = [
  {
    imageSrc: 'src/assets/images/rectangle1.png',
    altText: 'Texto alternativo 1',
    captionTitle: 'Título del banner 1',
    captionText: 'Descripción del banner 1'
  },
  {
    imageSrc: 'src/assets/images/rectangle2.png',
    altText: '2',
    captionTitle: 'Título del banner 1',
    captionText: 'Descripción del banner 1'
  },
  {
    imageSrc: 'src/assets/images/rectangle2.png',
    altText: 'Texto alternativo 1',
    captionTitle: 'Título del banner 1',
    captionText: 'Descripción del banner 1'
  },
];

const products = [
  {
    id: 1,
    name: 'blanco',
    imageUrl: 'src/assets/imagesP/image_3.png',
    temp: '190-220',
    precio: '$20',
    bedTep:'0-60'
  },
  {
    id: 2,
    name: 'negro',
    imageUrl: 'src/assets/imagesP/image_3.png',
    temp: '190-220',
    precio: '$20',
    bedTep:'0-60'
  },
];

export const Home = () => {
  const [searchQuery, setSearchQuery] = useState('');
  return (
    <>
    <CarouselBanner banners={bannersData} />
    <ProductGrid searchQuery={searchQuery} products={products} ></ProductGrid>
    <ContactForm apiUrl={null}></ContactForm>
    <FooterCus onSearch={setSearchQuery}></FooterCus>
    <FixedButtons></FixedButtons>
    </>
  )
}
