import { collection, getDocs } from 'firebase/firestore';
import React, { useEffect, useState } from 'react';
import { Col, Row } from 'react-bootstrap';
import { FirebaseDB } from '../../firebase/firebase';
import '../assets/product.css'; // Asegúrate de tener un archivo CSS con este nombre
import useWindowWidth from './useWindowWidth';

const ProductGrid = ({ searchQuery }) => {
  const [products, setProducts] = useState([]);
  const windowWidth = useWindowWidth(); // Use the custom hook

  const [hoveredProductId, setHoveredProductId] = useState(null);
  // Filter products based on search query
  const filteredProducts = products.filter(product => {
    const query = searchQuery.toLowerCase(); // Remove case sensitivity

    // Filter to all products where the name, price, or temperature includes the search query
    return (
      product.name.toLowerCase().includes(query) ||
      product.price.toString().includes(query) ||
      product.printTemp.max.toString().includes(query) ||
      product.printTemp.min.toString().includes(query) ||
      product.bedTemp.max.toString().includes(query) ||
      product.bedTemp.min.toString().includes(query)
    )
  });
  useEffect(() => {
    const fetchProducts = async () => {
      const querySnapshot = await getDocs(collection(FirebaseDB, 'filaments'));
      const productsList = querySnapshot.docs.map(doc => ({
        id: doc.id,
        ...doc.data(),
      }));
      setProducts(productsList);
    };

    fetchProducts();
  }, []);

  const showDetailsAlways = windowWidth < 768;

  // Handling hover only on larger screens
  const handleMouseEnter = (id) => {
    if (windowWidth >= 768) {
      setHoveredProductId(id);
    }
  };

  const handleMouseLeave = () => {
    if (windowWidth >= 768) {
      setHoveredProductId(null);
    }
  };

  return (
    <div className="product-grid" id="filaments">
      <h2 style={{ color: '#FF5206' }}>Filamento PLA Pro</h2>
      <p>1.75mm diámetro exterior</p>
      <p>1kg por rollo</p>
      <div className="products">
      <Row>
        {filteredProducts.map((product) => (
          <Col xs={12} sm={6} md={4} lg={3} key={product.id}>
            <div 
              className="product-item"
              onMouseEnter={() => handleMouseEnter(product.id)}
              onMouseLeave={handleMouseLeave}>
              <img src={product.imageUrl} alt={product.name} />
              {/* Show details based on screen size */}
              {(showDetailsAlways || product.id === hoveredProductId) && (
                <div className="product-details">
                  <h4>{product.name}</h4>
                  <p>Print Temp: {product.printTemp.min} - {product.printTemp.max}°C</p>
                  <p>Bed Temp: {product.bedTemp.min} - {product.bedTemp.max}°C</p>
                  <p>${product.price}</p>
                </div>
              )}
            </div>
          </Col>
        ))}
      </Row>

      </div>
    </div>
  );
};

export default ProductGrid;