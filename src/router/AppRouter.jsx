import React from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import { Home } from '../home/pages/Home'

export const AppRouter = () => {
  return (
    <>
    <Routes>
            <Route path="/" element={<Home />} />
            <Route path="*" element={<Navigate to="/" />} />
    </Routes>
    </>
  )
}
