import { loginWithEmailPassword } from "../../firebase/providers";
import { checkingCredentials, logout, login } from "./auth-slice";

export const checkingAuthentication = (email, password) =>{
    return async(dispatch) =>{
        dispatch(checkingCredentials());
    }
}

export const startLoginWithEmailPassword = ({ email, password }) => {
    return async( dispatch ) => {

        dispatch( checkingCredentials() );

        const result = await loginWithEmailPassword({ email, password });
        console.log(result);

        if ( !result.ok ) return dispatch( logout( result ) );
        dispatch( login( result ));

    }
}